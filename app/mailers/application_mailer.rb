class ApplicationMailer < ActionMailer::Base
  default from: "noreply@truvus.com"
  layout 'mailer'
end
